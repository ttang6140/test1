import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgifComponent } from './view/ngif/ngif.component';
import { NgSwitchComponent } from './view/ng-switch/ng-switch.component';
import { NgbindComponent } from './view/ngbind/ngbind.component';
import { NgstyleclassComponent } from './view/ngstyleclass/ngstyleclass.component';
import { NgmodelComponent } from './view/ngmodel/ngmodel.component';
import { FormsModule } from '@angular/forms';
import { NgforComponent } from './view/ngfor/ngfor.component';
import { ParentComponent } from './view/parent/parent.component';

@NgModule({
  //声明组件-管道符-指令
  declarations: [
    AppComponent,
    NgifComponent,
    NgSwitchComponent,
    NgbindComponent,
    NgstyleclassComponent,
    NgmodelComponent,
    NgforComponent,
    ParentComponent
  ],
  //模块-针对全局的
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  //注入服务
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
