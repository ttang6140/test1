import { Component } from '@angular/core';

@Component({
  selector: 'app-ngstyleclass',
  templateUrl: './ngstyleclass.component.html',
  styleUrls: ['./ngstyleclass.component.scss']
})
export class NgstyleclassComponent {
  title = 'I am a sodier'
  flagClass = true
  ObjStyle = {
    color: "red",
    background: "yellow",
    fontSize: "60px"
  }
}
