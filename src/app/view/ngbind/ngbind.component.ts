import { Component } from '@angular/core';

@Component({
  selector: 'app-ngbind',
  templateUrl: './ngbind.component.html',
  styleUrls: ['./ngbind.component.scss']
})
export class NgbindComponent {
  value = 'I am a content'
  dom = `<mark>I am a mark</mark>`
}
