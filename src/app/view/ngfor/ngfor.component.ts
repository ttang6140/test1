import { Component } from '@angular/core';

@Component({
  selector: 'app-ngfor',
  templateUrl: './ngfor.component.html',
  styleUrls: ['./ngfor.component.scss']
})
export class NgforComponent {
  numbers = [100, 2, 3, 5, 69, 99]
  list = [
    {
      name: "Beijing"
    },
    {
      name: "tianjin"
    },
    {
      name: "shanghai"
    },
  ]
}
